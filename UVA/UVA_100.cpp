#include <iostream>

using namespace std;

int main()
{
	int i, j;
	while (cin >> i >> j)
	{
		int s,e,max = 0;
		s = i;
		e = j;
		if (s > e)
		{
			int temp = s;
			s = e;
			e = temp;
		}

		for (int n = s; n <= e; n++)
		{
			int m, c;
			c = 1;
			m = n;
			while (m != 1)
			{
				if (m % 2 == 0)
					m = m / 2;
				else
					m = 3 * m + 1;
				c += 1;
			}
			if (c > max)
				max = c;
		}
		cout << i << ' ' << j << ' '<< max << '\n';
	}
	
}
